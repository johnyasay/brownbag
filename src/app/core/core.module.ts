import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { HttpService } from '../services/http.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  exports: [HttpClientModule],
  providers: [HttpService]
})
export class CoreModule { }
