import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  data: any;
  private dataLoaded: Promise<boolean>;

  constructor(private http: HttpService) { }

  ngOnInit() {
    this.dataLoaded = Promise.resolve(false);
    this.http.get('../assets/mock.json').subscribe(response => {
      this.data = response;
      setTimeout(() => {
        this.dataLoaded = Promise.resolve(true);
      }, 1000);
    });
  }

}
