import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-custombutton',
  templateUrl: './custombutton.component.html',
  styleUrls: ['./custombutton.component.css']
})
export class CustombuttonComponent implements OnInit {
  @Input() title: String;
  
  constructor() { }

  ngOnInit() {
  }

}
