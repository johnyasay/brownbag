import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustombuttonComponent } from './custombutton/custombutton.component';
import { CustomfieldComponent } from './customfield/customfield.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    CustombuttonComponent,
    CustomfieldComponent
  ],
  exports: [
    CustombuttonComponent,
    CustomfieldComponent
  ]
})
export class SharedModule { }
