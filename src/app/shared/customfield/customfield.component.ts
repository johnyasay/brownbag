import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-customfield',
  templateUrl: './customfield.component.html',
  styleUrls: ['./customfield.component.css']
})
export class CustomfieldComponent implements OnInit {
  @Input() label: String;
  @Input() type: String;

  constructor() { }

  ngOnInit() {
  }

}
